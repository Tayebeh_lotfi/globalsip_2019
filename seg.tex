\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
%\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

\usepackage{tipa}
%\usepackage[pdftex]{graphicx}
\usepackage{epsfig}
\usepackage{epstopdf}
\usepackage{enumitem}
\usepackage{times}
\usepackage{caption}
\captionsetup{font=footnotesize,justification=centering,labelsep=period}
\setcounter{tocdepth}{3}
\usepackage{url}
\usepackage{subfig}
\usepackage{dsfont}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{epstopdf}
\usepackage{ulem}
\usepackage{cite}
\usepackage{changepage}
\usepackage{multicol}

% Example definitions.
% --------------------
\def\x{{\mathbf x}}
\def\L{{\cal L}}
\usepackage{color}
\addtolength{\textheight}{-.7cm}
\def\X{{\mathbf X}}
\def\Y{{\mathbf Y}}
\def\A{{\mathbf A}}

% *** MATH PACKAGES ***
%
%\usepackage[cmex10]{amsmath}
% *** SUBFIGURE PACKAGES ***
%\usepackage{subfigure}

% correct bad hyphenation here
\hyphenation{op-tical net-works semi-conduc-tor}

% ---------------------------------------------------
% Begin of Document
% ---------------------------------------------------

\begin{document}

\title{Deep Learning Methods for Image Segmentation Containing Translucent Overlapped Objects}
%\thanks{}
%}

\author{\IEEEauthorblockN{Tayebeh Lotfi Mahyari} % 1\textsuperscript{st}
\IEEEauthorblockA{\textit{Department of Systems and Computer Engineering} \\
\textit{Carleton University}\\
Ottawa, Canada \\
tayebeh.lotfimahyari@carleton.ca}
\and
\IEEEauthorblockN{Richard M. Dansereau} % 2\textsuperscript{nd} 
\IEEEauthorblockA{\textit{Department of Systems and Computer Engineering} \\
\textit{Carleton University}\\
Ottawa, Canada \\
rdanse@sce.carleton.ca}
}

\maketitle


\begin{abstract}
\noindent Convolutional neural networks(CNN) are a subset of deep learning methods recently used widely for image segmentation. SegNet network~\cite{badrinarayanan} has shown interesting results for semantic segmentation, but it is designed to segment images with non-overlapped objects. However in some data translucent regions partially overlap. Having overlapped regions will cause methods not designed for overlapped objects to perform poorly or not work at all. To our knowledge no CNN has been designed yet to segment partially overlapped translucent objects. 

In this paper, we have designed a CNN to segment partially overlapped translucent regions. We used SegNet~\cite{badrinarayanan} as transfer learning for our overlapped image segmentation method. We also designed a new CNN with a simpler network for our data. Results on synthetic images give more than $95\%$ segmentation accuracy for both methods. 
\end{abstract}

% ---------------------------------------------------
% Keywords
% ---------------------------------------------------

\begin{IEEEkeywords}
Machine learning; deep learning; image segmentation; translucent overlapped images.
\end{IEEEkeywords}

\IEEEpeerreviewmaketitle

% ---------------------------------------------------
% Introduction
% ---------------------------------------------------

\section{Introduction}
\label{section:Intro}

Image segmentation is a process of partitioning an image into multiple parts or regions with similar features or properties. Image segmentation is widely used in different applications such as object detection~\cite{delmerico}, object recognition~\cite{ramadevi}, and disease diagnosis in medical imaging~\cite{wu1,deepa}. 

\indent Machine learning methods are widely used in image segmentation applications~\cite{jena}. A basic and fundamental step in common machine learning algorithms is extracting useful features from data. However it is not always easy to find a good set of features for a specific type of data and application and it is still an open research area.

\indent Deep neural networks in deep learning~\cite{cui,badea,tanwar} have shown to solve this problem with the cost of high learning time. Using the idea of the structure of brain, deep neural networks use complex structures and are fed a lot of information to train. Given enough training, these structures automatically find the important features and learn based on these extracted features. Deep learning has emerged as a new field of research in machine learning since 2006 and has flourished in the past decade as a powerful tool in many learning applications such as image segmentation. 

\indent One class of deep neural networks is convolutional neural networks (CNN)\cite{schmidhuber,haridas}. CNNs usually use feed-forward neural network structure because of simplicity~\cite{krizhevsk,simonyan,szegedy,zeiler}. CNNs typically consist of convolutional layers, pooling layers, fully connected layers and normalization layers~\cite{krizhevsk,simonyan,szegedy,zeiler,kaiming,badrinarayanan}. They use gradient based back-propagation methods for weight tuning. CNNs have been used widely in image processing fields such as computer vision, medical diagnosis, and robot remote controls~\cite{pouyanfar,aarthi}. 

\subsection{Image Segmentation using CNNs}
\label{segcnn}
The most common CNNs that are widely used for image segmentation are AlexNet~\cite{krizhevsk}, VGGNet~\cite{simonyan}, and GoogLeNet~\cite{szegedy}. 

\indent These structures were first implemented for image classification, however in~\cite{shelhamer} the authors modified these networks for semantic segmentation. They simply added upsampling layers to the network to increase the size of the intermediate layers that were decreased after convolutional layers and max pool layers. In this way they kept the size of the output layer the same as the size of the input image so that the learning could be done for segmentation. Following~\cite{shelhamer}, authors in~\cite{badrinarayanan} modified VGGNet~\cite{simonyan} to use transfer learning for image segmentation application.

\indent These networks are trained with thousands of different types of image data to make a standard network for many applications, therefore the number of layers and weights is large.

\indent To do image segmentation using CNNs, there are two different ways:

\begin{enumerate}[label=\arabic*] % [label=(\roman*)]
\item Implement one's own structure and tune the weights using the input/output learning phase. In this approach, the network will be application dependent and the number of layers and weights can be reduced because of the specific type of data that is going to be processed. However, since there are a large number of weights in a CNN, this approach needs a lot of time and data during training. Therefore choosing this approach over transfer learning is dependent on the application~\cite{tajbakhsh}.

\item Start with a pre-trained network and augment the training with application specific data, which is called transfer learning. This approach is often preferred since one can save on the amount of data needed and the learning time.
\end{enumerate}

\indent The previous CNNs designed for semantic image segmentation assume image data with non-overlapped objects. However in some data such as cytology images or X-Ray images, translucent regions partially overlap. Having overlapped regions will cause methods not designed for overlapped objects to perform poorly or not work at all. To our knowledge no CNN has been designed yet to segment partially overlapped translucent objects. 

In this paper, we focus on using CNNs for image segmentation, but where the images include overlapping images features from translucent objects. We have done both transfer learning and learning from scratch. We used the SegNet network~\cite{badrinarayanan} for transfer learning. We also implemented a network with $31$ layers~\cite{tayebeh2} and trained it from the scratch. 
Our proposed CNN is outlined in Sec.~\ref{propseg}. Methods explanation to create the synthetic datasets comes in Sec.~\ref{setup1}. Sec.~\ref{res} evaluates the proposed method and conclusions and future work are given in Sec.~\ref{conclution}.

\section{Proposed CNNs}
\label{propseg}
SegNet network~\cite{badrinarayanan} is a deep neural network with $91$ layers that is a modified version of VGGNet~\cite{simonyan} for image segmentation. It has an encoder network, a decoder network and a pixelwise classification layer. The encoder network consists of $13$ convolutional layers with a kernel size of $3 \times 3$. Each convolutional layer is followed by a batch normalization layer, an element wise rectified-linear unit (ReLU) and a max-pooling layer with $2 \times 2$ window size and stride of $2$. To restore the size of the image, for each max-pooling in the encoder, there is an un-pooling layer at the decoder network. Each max-unpooling layer is followed by a convolutional layer with kernel size of $3 \times 3$, a batch normalization layer, and a ReLU layer. The network is finally followed by a soft-max pixel classification layer. Images of size $360 \times 480$ are fed into the network. The output of the network is a $360 \times 480 \times K$ image where each layer represents the probability of each of $K$ classes for each pixel.

\indent Because implementing a new network would need a large amount of data and time, we transferred our data to SegNet to save training time. Since our application is to segment partially overlapped images, we replaced the max operator layer in SegNet with a threshold layer in order to be able to use the SegNet network for our dataset. The output of the network is a multi-layer image where each image layer represents one object, where some of these objects may have overlapped in the original image. 

\indent We also propose a simpler network with $71$ layers. Similar to SegNet network, our proposed network consists of an encoder network, a decoder network and a soft-max pixel classification layer. Our encoder network has four sets of layers. Each set has $2$ or $3$ convolutional layers with kernel size of $3 \times 3$ followed by a batch normalization layer and a ReLU layer. At the end of each set we added a max-pooling layer with $2 \times 2$ window size and stride of $2$. Similar to the encoder network, our decoder network has four sets of layers. Each set of layers start with a max-unpooling layer followed by a convolutional layer with kernel size $3 \times 3$, a batch normalization layer and a ReLU layer. A soft-max classification layer at the end provides the multi-layer image segmentation output. Figure~\ref{fig:propnet} shows the structure of our proposed network with $71$ layers.

\begin{figure}[t]
\includegraphics[width=3.5in]{./Results/proposed1.pdf}
\vspace{-45mm}
\caption{Proposed network for semantic segmentation.}\label{fig:propnet}
\end{figure}

\indent However SegNet and the proposed network are slow. To improve the time complexity of our proposed network, we added residual modules to it. We also changed softmax layer to sigmoid layer in order to improve the multi-label image segmentation accuracy (Figure~\ref{fig:propnet2}). 

\begin{figure}[t]
\includegraphics[width=3.5in]{./Results/proposed2.pdf}
\vspace{-45mm}
\caption{Proposed residual network for semantic segmentation.}\label{fig:propnet2}
\end{figure}

\section{Data Set-up for Overlapped Image Segmentation}
\label{setup1}
We built three different synthetic datasets. 
\begin{enumerate}[label=\arabic*]
\item A database of $700$ images including $420$ training images and $280$ test images was created. We created distorted geometrical shapes, such as circles, triangles, rectangles and squares, and we added four different textures from~\cite{texture} to these objects to form the image I as
\begin{equation}\label{eq:ptrn1}
I = \Sigma a_{ml} \times O_m * P_m(x,y), \quad m = 1,2,3,4,
\end{equation}
where $O_m$ is the $m_{th}$ object, $*$ represents the convolution operation, $P_m$ is the $m_{th}$ pattern and $a_{ml}, m = 1,2,3,4$ is a weighting for a linear combination where
\begin{equation}\label{eq:vrblmtx2}
a_{m,l} = a_m + N_l(0,\sigma), \qquad N_l = \frac{1}{\sigma \sqrt{2\pi}} e^{\frac{l^2}{2\sigma^2}}
\end{equation}
where $l\in[0,1]$ is a random number and $\sigma$ is the power of the added noise. 

\indent For each mixing image in the database, a four-layered image label was created, each layer containing the black-and-white map of one of the original shapes. Therefore the layers are allowed to have overlap, but each layer contains the map of its own object.

\item A dataset of $720$ images including $432$ training images and $288$  test images was created. Similar to the previous dataset, four distorted geometrical shapes with four different textures from~\cite{texture} were created~(\ref{eq:ptrn1}). However we chose randomly two or three or four of the components in the mixing matrix $a_{ml}$ and put the others to zero. In other words, the mixing matrix $a_{ml}$ is considered as
\[
    a_{m,l}= 
\begin{cases}
    a_m + N_l(0,\sigma),& \text{if the component is selected} \\
    0,              & \text{otherwise}
\end{cases}
\]
where $N_l = \frac{1}{\sigma \sqrt{2\pi}} e^{\frac{l^2}{2\sigma^2}}$, $l\in[0,1]$ is a random number, and $\sigma$ is the power of the added noise. 

\indent Therefore we created a set of images mixed of two, three or four objects. Labels were created similar to the previous dataset. 

\item We created a dataset of $50000$ images including $40000$ training images and $10000$ test images of size $256 \times 256$. 

\indent We first picked $100$ cells from cytology images and created the ground truth segmentation. Then we applied an affine transform to each cell to generate a variation over the dataset
\begin{equation}
F(p) = A p + q
\end{equation}
where $p \in O_m $, $A$ is a linear transformation in $\mathbb{R}^2$, $q$ is a displacement vector in $\mathbb{R}^2$, and $O_m$ is the $m_{th}$ object $m=1,2,\ldots,100$. 

\indent We then created $2000$ images each having $12$ of these transformed cells. We finally for $50000$ times picked three images randomly each time and combined them using alpha blending method~(\ref{eq:blend1})-(\ref{eq:blend5}) to have a translucent overlapped image. 
\begin{equation}
\label{eq:blend1}
I_m = \alpha J_{mnp} + \beta K_m
\end{equation}
\begin{equation}
\label{eq:blend2}
\begin{split}
J_{mnp} =\{ Q_m|Q_m = O_{mn} \cap O_{mp}, \\
O_{mn} \in I_{mn}, O_{mp} \in I_{mp} \}
\end{split}
\end{equation}
\begin{equation}
\label{eq:blend3}
\begin{split}
K_m = \{ Q_m| Q_m = O_{m1} \cap O_{m2} \cap O_{m3}, \\ O_{m1} \in I_{m1}, O_{m2} \in I_{m2}, O_{m3} \in I_{m3} \}
\end{split}
\end{equation}
where $O_{mn}, O_{mp}$ represent objects in $n_{th}$ and $p_{th}$ images at $m_{th}$ iteration respectively, and $I_{mn}, I_{mp}$ are the $n_{th}$ and $p_{th}$ images at $m_{th}$ iteration respectively with $m=1,2,\ldots,50000$, $n,p=1,2,3$
and 
\begin{equation}
\label{eq:blend4}
\alpha = \left\{ \begin{array}{lll}
        0.45 & \quad \mbox{for $I_1$},\\
        0.35 & \quad \mbox{for $I_2$},\\
        0.45 & \quad \mbox{for $I_3$},\end{array} \right.
\end{equation} 

\begin{equation}
\label{eq:blend5}
\beta = \left\{ \begin{array}{lll}
        0.2 & \quad \mbox{for $I_1$},\\
        0.12 & \quad \mbox{for $I_2$},\\
        0.2 & \quad \mbox{for $I_3$}.\end{array} \right.
\end{equation} 
are the coefficients for blending.

The labels consists of layers with segmentation of objects, each layer with non-overlapped objects. 
\end{enumerate}

Figure~\ref{fig:database1} shows a few samples of all three datasets.

\begin{figure}[t]
\includegraphics[width=3.5in]{./Results/database1_3_v3.eps}
%\vspace{-15mm}
\caption{one synthetic image for each dataset. Left: first dataset, middle: second dataset, right: third dataset.}\label{fig:database1}
\end{figure}

\section{Results}
\label{res}
For transferring the SegNet network~\cite{badrinarayanan} to our dataset, we trained SegNet on Matlab platform with $50$ epochs using $700$ learning data of size $360 \times 480$. We also trained our proposed network for first and second datasets on Matlab platform with $50$ epochs using $700$ and $720$ learning data of size $200 \times 200$ respectively. For the third dataset we trained our proposed network on Python platform with $50$ epochs using $50000$ learning data of size $256 \times 256$. We used learning rate $0.001$ and Adam optimizer~\cite{adam} with first and second momentum parameters $\beta_1 = .9$ and $\beta_2 = .999$ on both Matlab and python platforms. Figures~(\ref{fig:datares1}-\ref{fig:datares2}) show the results for the proposed method over the second and third datasets.

\begin{figure}[t]
\includegraphics[width=3.5in]{./Results/Mynet2_4classbckg50epch720smpl2.eps}
\vspace{-5mm}
\caption{Results for the proposed network on the second dataset. Top: segmentation results, middle: ground truth segmentation, bottom: original mixture and sources.}\label{fig:datares1}
\end{figure}

\begin{figure}[t]
\includegraphics[width=3.7in]{./Results/Cytobckg50epoch50000smpl2.eps}
\vspace{-5mm}
\caption{Results for the proposed network on the third dataset. Left: original mixture, middle: segmentation results, right: ground truth.}\label{fig:datares2}
\end{figure}

Figure~\ref{fig:datares1} shows the segmentation results of the second dataset for our proposed network. The first row represents the output segmentation, the second row represents the ground truth segmentation and the third row represents the input image and the four original sources. As can be seen in the third row, the input image has three sources and the second source is set to zero. In the first and second rows, the second segment is set to blank as there are only three sources.

Figure~\ref{fig:datares2} represents the segmentation results for the third dataset. The first image represents the original image mixture with overlapped regions. The third image in the figure which is the ground truth segmentation of the mixture, represents the segmentation results in three image layers. Starting from the first image layer, the objects that are overlapped with first layer objects are moved to the second layer and the overlapped objects with the first and second layers are moved to the third image layer. The middle image in the figure represents the segmentation results. In this figure red, green, and blue colors for second and third images represent objects in first, second, and third image segmentation layers respectively.

\indent We calculated the segmentation accuracy by 
\begin{equation}\label{eq:acc}
Acc = \frac{|\Sigma_G = \Sigma_R|}{|V|} \times 100\%
\end{equation} 
where $\Sigma_G = \Sigma_R$ is a set of nodes where the ground truth segmentation $\Sigma_G$ is the same as the results $\Sigma_R$ and $|V|$ is the total number of pixels in the image.

\indent We also measured the intersection of union (IoU) by
\begin{equation}\label{eq:iou}
IoU = \frac{\Sigma_G \cap \Sigma_R}{\Sigma_G \cup \Sigma_R} \times 100\%
\end{equation}
where $\cap$ and $\cup$ represent intersection and union of ground truth segmentation and segmentation results respectively.

\indent Table~\ref{tbl:acc1} shows the accuracy of the segmentation results calculated by~(\ref{eq:acc}) and~(\ref{eq:iou}) for all three datasets. For the first two datasets the results are averaged over $20$ different trials. For the third dataset the results are averaged over $100$ different trials.

\begin{table}[t]
\caption{Segmentation accuracy.}\label{tbl:acc1}
\begin{center}
\begin{tabular}{ | c | c | c | }
\hline
Description & $Accuracy(\%)$ & $IoU(\%)$ \\ \hline
$Transfer\_{First}$ & 98.136089 & 95.131851 \\ \hline
$Proposed1\_{First}$ & 99.055875 & 97.484121 \\ \hline
$Proposed1\_{Second}$ & 99.371025 & 97.593753 \\ \hline
$Proposed2\_{Second}$ & 99.551796 & 98.065873 \\ \hline
$Proposed2\_{Third}$ & 99.784058  & 95.811349 \\ \hline
\end{tabular}
\end{center}
\end{table}

\indent In table ~(\ref{tbl:acc1}), $Transfer$, $Proposed1$ and $Proposed2$ represent transfer learning, proposed network and proposed residual network respectively. $First$, $Second$, and $Third$ represent the three datasets respectively.

\indent By comparing results in table~(\ref{tbl:acc1}), it is seen that the performance of the transformed network is very good. But when we use the proposed network the accuracy increases slightly with the same number of training data. It is because the proposed network starts with the random weights while the pre-trained network has a initial start to segment non-overlapped objects. Therefore the accuracy of pre-trained network to segment overlapped data is less. We applied the proposed network and proposed residual network on the second dataset. The accuracy of the proposed residual network increases slightly while the time complexity is $2.5$ times less than the first proposed network.

\indent Because of the better accuracy and time complexity of the proposed residual network, we used the residual network for the third dataset. We created much more complex images that can be close to the read datasets of cytology images. Therefore the number of training images and the training time is increased. 

\section{Conclusion} % and Future Work}
\label{conclution}
In this paper we used deep learning to segment translucent partially overlapped objects. Although deep learning is widely used for segmentation applications recently, we believe that there is no work done for overlapped objects segmentation. We used a pre-trained network called SegNet network~\cite{badrinarayanan} and modified it to our application. We also implemented two convolutional neural networks and calculated the segmentation accuracy for three datasets. Results for the synthetic data show better segmentation accuracy for our proposed networks comparing to SegNet~\cite{badrinarayanan}.

%\indent In the future, we intend to apply our method on real overlapped objects.

\bibliographystyle{plain}%IEEETrans
\addcontentsline{toc}{chapter}{Bibliography}
\typeout{Bibliography}
\bibliography{refs}

\end{document}
